/**
 *
 * @param {array} arr
 */

const insertionSort = (arr) => {
  const copy = arr.slice();

  for (let i = 1; i < copy.length; i++) {
    const currVal = copy[i];

    for (let j = i - 1; j >= 0; j--) {
      if (copy[j] > copy[j + 1]) {
        copy[j + 1] = copy[j];
        copy[j] = currVal;
      } else {
        break;
      }
    }
  }

  return copy;
};

console.log(insertionSort([1, 2, 9, 76, 0]));
console.log(
  insertionSort([87, 49, 12, 37, 99, 15, 8, 74, 27, 64, 85, 2, 25, 16, 50])
);
