const sortEx3Numbers = (arr, temp) => {
  const copy = arr.slice();

  if (copy[1] < copy[0]) {
    temp[0] = copy[0];
    copy[0] = copy[1];
    copy[1] = temp[0];
  }

  if (copy[2] < copy[1]) {
    temp[0] = copy[1];
    copy[1] = copy[2];
    copy[2] = temp[0];

    if (copy[1] < copy[0]) {
      temp[0] = copy[0];
      copy[0] = copy[1];
      copy[1] = temp[0];
    }
  }

  return copy;
};

const arr = [10, 7, 5];
const temp = [arr[0]];
console.log(sortEx3Numbers(arr, temp));
