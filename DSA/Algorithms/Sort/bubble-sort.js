/**
 *
 * @param {array} numbers
 */

const bubbleSort = (numbers) => {
  const copyOfNumbers = numbers.slice();

  // * a naive way
  // for (let i = 0; i < copy.length; i++) {
  //   for (let j = 0; j < copy.length; j++) {
  //     const currVal = copy[j];

  //     if (currVal > copy[j + 1]) {
  //       copy[j] = copy[j + 1];
  //       copy[j + 1] = currVal;
  //     }
  //   }
  // }

  // a bit optimized way
  //   for (let i = copy.length; i > 0; i--) {
  //     for (let j = 0; j < i - 1; j++) {
  //       const currVal = copy[j];

  //       if (currVal > copy[j + 1]) {
  //         copy[j] = copy[j + 1];
  //         copy[j + 1] = currVal;
  //       }
  //     }
  //   }

  return copyOfNumbers;
};

// console.log(bubbleSort([17, 7, 21, 26, 11]));
console.log(bubbleSort([3, 1, 5, 2, 4]));
