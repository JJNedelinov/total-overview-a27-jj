class SinglyLinkedListNode {
  constructor(value, next) {
    this.value = value || value === 0 ? value : null;
    this.next = next ? next : null;
  }
}

export default SinglyLinkedListNode;
