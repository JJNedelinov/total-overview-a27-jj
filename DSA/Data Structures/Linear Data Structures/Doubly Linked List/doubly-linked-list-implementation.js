import DoublyLinkedListNode from '../doubly-linked-list-node.js';

class DoublyLinkedList {
  #head;
  #tail;
  #count = 0;

  /**
   *
   * @param {DoublyLinkedListNode} head
   * @param {DoublyLinkedListNode} tail
   */
  constructor(head, tail) {
    if (head) {
      this.#head = head;
      this.#count++;

      if (tail) {
        this.#tail = tail;
        this.#head.next = this.#tail;
        this.#tail.prev = this.#head;

        this.#count++;
      } else {
        this.#tail = head;
      }
    } else {
      this.#head = this.#tail = null;
    }
  }

  get head() {
    return this.#head;
  }

  get tail() {
    return this.#tail;
  }

  get count() {
    return this.#count;
  }

  addFirst(value) {
    if (!this.#head) {
      return this.#addHeadTail(value);
    }

    if (this.#count === 1) {
      return this.#separateAddFirst(value);
    }

    const node = new DoublyLinkedListNode(value, this.#head);
    this.#head.prev = node;
    this.#head = node;
    this.#count++;
    return this.#head;
  }

  addLast(value) {
    if (!this.#head) {
      return this.#addHeadTail(value);
    }

    if (this.#count === 1) {
      return this.#separateAddLast(value);
    }

    const node = new DoublyLinkedListNode(value, null, this.#tail);
    this.#tail.next = node;
    this.#tail = node;
    this.#count++;
    return this.#tail;
  }

  removeFirst() {
    if (!this.#head) {
      // throw error

      return null;
    }

    if (this.#count === 1) {
      const node = this.#head;
      this.#head = this.#tail = null;
      this.#count++;
      return node;
    }

    const node = this.#head;
    this.#head = this.#head.next;
    this.#head.prev = null;
    this.#count++;
    return node;
  }

  removeLast() {
    if (!this.#head) {
      // throw error

      return null;
    }

    if (this.#count === 1) {
      const node = this.#head;
      this.#head = this.#tail = null;
      this.#count--;
      return node;
    }

    const node = this.#tail;
    this.#tail = this.#tail.prev;
    this.#tail.next = null;
    this.#count--;
    return node;
  }

  insertBefore(node, value) {
    if (!node) {
      // throw error

      return null;
    }

    const newNode = new DoublyLinkedListNode(
      value,
      node,
      node.prev ? node.prev : null
    );

    if (node === this.#head) {
      node.prev = newNode;
      this.#head = newNode;
      return newNode;
    }

    node.prev.next = newNode;
    node.prev = newNode;
    this.#count++;
    return newNode;
  }

  insertAfter(node, value) {
    if (!node) {
      // throw error

      return null;
    }

    const newNode = new DoublyLinkedListNode(
      value,
      node.next ? node.next : null,
      node
    );

    if (node === this.#tail) {
      node.next = newNode;
      this.#tail = newNode;
      return newNode;
    }

    node.next.prev = newNode;
    node.next = newNode;
    this.#count++;
    return newNode;
  }

  find(value) {
    let top = this.#head;

    while (top.value !== value) {
      top = top.next;
    }

    return top;
  }

  values() {
    let top = this.#head;
    const values = [];

    while (top) {
      values.push(top.value);
      top = top.next;
    }

    return values;
  }

  nodes() {
    let top = this.#head;
    const nodes = [];

    while (top) {
      nodes.push(top);
      top = top.next;
    }

    return nodes;
  }

  #addHeadTail(value) {
    const node = new DoublyLinkedListNode(value);
    this.#head = this.#tail = node;
    this.#count++;
    return this.#head;
  }

  #separateAddFirst(value) {
    const node = new DoublyLinkedListNode(value, this.#head);
    this.#tail.prev = node;
    this.#head = node;
    this.#count++;
    return node;
  }

  #separateAddLast(value) {
    const node = new DoublyLinkedListNode(value, null, this.#head);
    this.#head.next = node;
    this.#tail = node;
    this.#count++;
    return node;
  }
}

const head = new DoublyLinkedListNode(-10);
const tail = new DoublyLinkedListNode(10);

// const doublyLinkedList = new DoublyLinkedList();
// const doublyLinkedList = new DoublyLinkedList(head);
const doublyLinkedList = new DoublyLinkedList(head, tail);

doublyLinkedList.addFirst(1);
doublyLinkedList.addFirst(2);
doublyLinkedList.addLast(3);
doublyLinkedList.addLast(4);

console.log(doublyLinkedList.values());
console.log(doublyLinkedList.nodes());

doublyLinkedList.removeFirst();
doublyLinkedList.removeLast();

console.log(doublyLinkedList.values());
console.log(doublyLinkedList.nodes());

const testNode1 = doublyLinkedList.find(3);

doublyLinkedList.insertAfter(testNode1, 200);

console.log(doublyLinkedList.values());
console.log(doublyLinkedList.nodes());

const testNode2 = doublyLinkedList.find(1);

doublyLinkedList.insertBefore(testNode2, 2);

console.log(doublyLinkedList.values());
console.log(doublyLinkedList.nodes());
