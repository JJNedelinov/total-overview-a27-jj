import SinglyLinkedListNode from '../singly-linked-list-node.js';

class Queue {
  #head;
  #tail;
  #count = 0;

  /**
   *
   * @param {SinglyLinkedListNode} head
   * @param {SinglyLinkedListNode} tail
   */
  constructor(head, tail) {
    if (head) {
      this.#head = head;
      this.#count++;

      if (tail) {
        this.#tail = tail;
        this.#head.next = this.#tail;

        this.#count++;
      } else {
        this.#tail = head;
      }
    } else {
      this.#head = this.#tail = null;
    }
  }

  get head() {
    return this.#head;
  }

  get tail() {
    return this.#tail;
  }

  enqueue(value) {
    const node = new SinglyLinkedListNode(value);
    this.#count++;

    if (!this.#head) {
      this.#head = this.#tail = node;
    } else {
      this.#tail.next = node;
      this.#tail = node;
    }

    return this.#tail;
  }

  dequeue() {
    if (!this.#head) {
      return null;
    }

    this.#count--;
    const value = this.#head.value;
    this.#head = this.#head.next;

    return value;
  }

  count() {
    return this.#count;
  }

  isEmpty() {
    return !!!this.#count;
  }

  values() {
    let top = this.#head;
    const values = [];

    while (top) {
      values.push(top.value);
      top = top.next;
    }

    return values;
  }

  nodes() {
    let top = this.#head;
    const nodes = [];

    while (top) {
      nodes.push(top);
      top = top.next;
    }

    return nodes;
  }
}

// const queue = new Queue();

// testing the algorithm for starting with some nodes (head/tail)
const head = new SinglyLinkedListNode(0);
const tail = new SinglyLinkedListNode(0.5);
const queue = new Queue(head, tail);

queue.enqueue(1);
queue.enqueue(2);
queue.enqueue(3);

console.log(queue.values());

queue.dequeue();

console.log(queue.values());

console.log(queue.isEmpty());
console.log(queue.count());
console.log(queue.nodes());
