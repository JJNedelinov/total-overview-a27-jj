// THIS IMPLEMENTATIONS ARE FOR DYNAMIC ARRAYS

const noClass = () => {
  const queue = [];

  // enqueue
  queue.push(1);
  queue.push(3);
  queue.push(5);
  queue.push(7);

  console.log(queue); // [1,3,5,7]

  // dequeue
  queue.shift();
  queue.shift();

  console.log(queue); // [5,7]

  // peek
  console.log(queue[0]); // 5

  // count
  console.log(queue.length); // 2

  // isEmpty
  console.log(!queue.length); // false

  // head
  console.log(queue[0]);

  // tail
  console.log(queue[queue.length - 1]);
};

// noClass();

// implemenatiton with Class

class Queue {
  #head;
  #tail;
  #queue;

  /**
   *
   * @param {array} data
   */
  constructor(data) {
    this.#head = data && data.length ? data[0] : null;
    this.#tail = data && data.length ? data[data.length - 1] : null;
    this.#queue = data && data.length ? data.slice() : [];
  }

  get head() {
    return this.#head;
  }

  get tail() {
    return this.#tail;
  }

  get values() {
    return this.#queue;
  }

  enqueue(value) {
    this.#queue.push(value);
    this.#tail = this.#queue[this.#queue.length - 1];

    return this.#tail;
  }

  dequeue() {
    const value = this.#queue.shift();
    this.#head = this.#queue[0];

    return value;
  }

  peek() {
    return this.#head;
  }

  count() {
    return this.#queue.length;
  }

  isEmpty() {
    return !!!this.#queue;
  }
}

const withClass = () => {
  const queue = new Queue([1, 2, 3, 4]);
  // const queue = new Queue();

  console.log(queue.head); // 1
  console.log(queue.tail); // 4
  console.log(queue.values); // [1, 2, 3, 4]

  console.log(queue.dequeue()); // 1
  console.log(queue.head); // 2

  console.log(queue.enqueue(5)); // 5
  console.log(queue.tail); // 5

  console.log(queue.values); // [2, 3, 4, 5]

  console.log(queue.count()); // 4
};

withClass();
