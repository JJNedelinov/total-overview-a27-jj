import SinglyLinkedListNode from '../singly-linked-list-node.js';

class Stack {
  #top;
  #count = 0;

  /**
   *
   * @param {SinglyLinkedListNode} node - which is an instance of SinglyLinkedListNode
   */
  constructor(node) {
    if (node) {
      count++;
      this.#top = node;
    } else {
      this.#top = null;
    }
  }

  push(value) {
    if (!this.#top) {
      const node = new SinglyLinkedListNode(value, null);
      this.#top = node;
      return;
    }

    const node = new SinglyLinkedListNode(value, this.#top);
    this.#top = node;
  }

  pop() {
    if (!this.#top) {
      return null;
    }

    if (!this.#top.next) {
      const value = this.#top.value;
      this.#top = null;

      return value;
    }

    const node = this.#top.next;
    const value = this.#top.value;
    this.#top = node;

    return value;
  }

  peek() {
    return this.#top;
  }

  count() {
    let count = 0;
    let top = this.#top;

    while (top) {
      count++;
      top = top.next;
    }

    return count;
  }

  isEmpty() {
    return !this.#top;
  }

  values() {
    let values = [];
    let top = this.#top;

    while (top) {
      values.push(top.value);
      top = top.next;
    }

    return values;
  }

  nodes() {
    let nodes = [];
    let top = this.#top;

    while (top) {
      nodes.push(top);
      top = top.next;
    }

    return nodes;
  }
}

const stack = new Stack(new SinglyLinkedListNode(1));
// const stack = new Stack();

console.log(stack.peek()); // null;

console.log(stack.values()); // [1]
console.log(stack.nodes()); // [SinglyLinkedListNode { value: 1, next: null }]

console.log(stack.isEmpty()); // false

console.log(stack.count()); // 1

stack.push(2);

console.log(stack.values()); // [2, 1]
console.log(stack.nodes());

console.log(stack.pop());

console.log(stack.values());

console.log(stack.pop());

console.log(stack.values());

console.log(stack.pop());

console.log(stack.values());

/* 
[
  SinglyLinkedListNode { value: 2, next: [SinglyLinkedListNode] },
  SinglyLinkedListNode { value: 1, next: null }
]
*/
