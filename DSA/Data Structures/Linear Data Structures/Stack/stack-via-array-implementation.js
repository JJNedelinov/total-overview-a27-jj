// Implemented via Array (no Class)

const noClass = () => {
  const stack = [];

  stack.push(1);

  console.log(stack); // [1]

  stack.push(21);
  stack.push(14);

  console.log(stack); // [1, 21, 14]

  stack.pop();
  stack.pop();

  console.log(stack); // [1]

  console.log(stack[0]); // 1

  return null;
};

// noClass();

// Implemented via Array (with Class)

class Stack {
  #stack;

  constructor() {
    this.#stack = [];
  }

  get stack() {
    return this.#stack.slice(0);
  }

  push(value) {
    this.#stack.push(value);
    return this.stack;
  }

  pop() {
    this.#stack.pop();
    return this.stack;
  }

  peek() {
    return this.stack[0];
  }
}

const withClass = () => {
  const s1 = new Stack();

  console.log(s1.stack); // []

  s1.push(1);
  s1.push(2);
  s1.push(3);

  console.log(s1.stack); // [1,2,3]

  s1.pop();

  console.log(s1.stack); // [1,2]
  console.log(s1.peek()); // 1
};

withClass();
