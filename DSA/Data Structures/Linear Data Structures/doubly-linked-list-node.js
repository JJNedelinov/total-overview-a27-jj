class DoublyLinkedListNode {
  constructor(value, next, prev) {
    this.value = value || value === 0 ? value : null;
    this.next = next ? next : null;
    this.prev = prev ? prev : null;
  }
}

export default DoublyLinkedListNode;
