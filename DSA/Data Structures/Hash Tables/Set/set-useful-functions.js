// check if all elements are unique

/**
 *
 * @param {array | string} val
 */
const uniqueElementsCheck = (val) => new Set(val).size === val.length;

// remove duplicates

/**
 *
 * @param {array | string} val
 */
const removeDuplicates = (val) =>
  // (val instanceof String) - this cannot be checked if val is not created by the String class -> new String('some value');

  !(val instanceof Array) && typeof val !== 'string'
    ? null
    : val instanceof Array
    ? [...new Set(val)]
    : [...new Set(val)].join('');

// union

/**
 *
 * @param  {... array | string | Set} vals
 * @returns
 */
const union = (...vals) => [
  ...new Set(vals.reduce((acc, curr) => acc.concat(...curr), [])),
];

// intersection

/**
 *
 * @param {Set} a - first set
 * @param {Set} b - second set
 * @returns the similarities of the both sets
 */
const intersection = (a, b) => {
  const set = new Set();

  for (const el of a) {
    if (b.has(el)) set.add(el);
  }

  return [...set];
};

// difference of Sets

/**
 *
 * @param {Set} a - first set
 * @param {Set} b - second set
 * @returns the differences of the both sets
 */
const difference = (a, b) => [...new Set([...a].filter((el) => !b.has(el)))];

// symmetric difference of Sets - the unique values for the sets which are also not in the other set

/**
 *
 * @param {Set} a - first set
 * @param {Set} b - second set
 * @param {Set} c - third set - the differences
 * @returns the differences of the both sets
 */
const symmetricDifference = (a, b, c) =>
  [...c].filter((val) => !a.has(val) && !b.has(val));

// * TESTING

// unique elements
console.log(uniqueElementsCheck([1, 2, 3, 4]));
console.log(uniqueElementsCheck([1, 2, 3, 3]));
console.log(uniqueElementsCheck('abcc'));
console.log(uniqueElementsCheck('abc'));
console.log(uniqueElementsCheck([{}, {}, {}]));

const a = {};
const b = {};
console.log(uniqueElementsCheck([{}, a, b]));
console.log(uniqueElementsCheck([{}, a, a]));

// remove duplicates
console.log(removeDuplicates([1, 2, 3, 4, 5]));
console.log(removeDuplicates([1, 2, 3, 4, 4]));
console.log(removeDuplicates('abccb'));
console.log(removeDuplicates('abc'));

// union
console.log(union([1, 2, 3, 4, 5], [1, 2, 3, 6, 7, 8, 9], '12345'));
console.log(union(new Set([1, 2, 3]), new Set([3, 4, 5])));

// intersection
console.log(intersection(new Set([1, 2, 3]), new Set([3, 4, 5])));
console.log(intersection(new Set([1, 2, 3]), new Set([6, 4, 5])));

// difference
console.log(difference(new Set([0, 1, 2, 3, 5]), new Set([3, 4, 5, 6])));

// symmetric difference
console.log(
  symmetricDifference(
    new Set([1, 3, 5, 7, 9]),
    new Set([2, 3, 4, 6]),
    new Set([10, 2, 4, 6, 8])
  )
);
