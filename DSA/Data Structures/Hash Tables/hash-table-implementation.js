class HashTable {
  #table;
  #size;

  constructor() {
    this.#table = new Array(127);
    this.#size = 0;
  }

  get table() {
    return this.#table;
  }

  get size() {
    return this.#size;
  }

  #hash(key) {
    key = key.toString();
    let hash = 0;

    for (let i = 0; i < key.length; i++) {
      hash += key[i].charCodeAt(0);
    }

    return hash % this.#table.length;
  }

  set(key, value) {
    const index = this.#hash(key);

    if (this.#table[index]) {
      for (let i = 0; i < this.#table[index].length; i++) {
        if (this.#table[index][i][0] === key) {
          // overwrite the value for the matching key with new value
          this.#table[index][i][1] = value;
          return;
        }
      }
      this.#table[index].push([key, value]);
    } else {
      this.#table[index] = [];
      this.#table[index].push([key, value]);
    }

    this.#size++;
  }

  get(key) {
    const index = this.#hash(key);
    if (this.#table[index]) {
      for (let i = 0; i < this.#table[index].length; i++) {
        if (this.#table[index][i][0] === key) {
          return this.#table[index][i][1];
        }
      }
    }

    return undefined;
  }

  delete(key) {
    const index = this.#hash(key);

    if (this.#table[index] && this.#table[index].length) {
      for (let i = 0; i < this.#table[index].length; i++) {
        if (this.#table[index][i][0] === key) {
          this.#table[index].splice(i, 1);
          this.#size--;
          return true;
        }
      }
    }

    return false;
  }

  display() {
    this.#table.forEach((values, index) => {
      const chainedValues = values.map(
        ([key, value]) => `[ ${key}: ${value} ]`
      );
      console.log(`${index}: ${chainedValues}`);
    });
  }
}

const ht = new HashTable();

ht.set('France', 111);
ht.set('Spain', 150);
ht.set('ǻ', 192);

ht.display();
// 83: [ France: 111 ]
// 126: [ Spain: 150 ],[ ǻ: 192 ]

console.log(ht.size); // 3

ht.delete('Spain');
ht.display();
// 83: [ France: 111 ]
// 126: [ ǻ: 192 ]

console.log(ht.table);

// * TESTING THE HASHING FN

// for (let i = 0; i <= 100; i++) {
//   ht.add('jj');
// }

// const set = new Set(Object.keys(ht.values)).size;
// console.log(Object.keys(ht.values).length === set);
// console.log(ht.test);
