// counting occurrences

/**
 *
 * @param {array} arr
 */
const countOccurrences = (arr) =>
  arr.reduce(
    (acc, curr) => acc.set(curr, acc.has(curr) ? acc.get(curr) + 1 : 1),
    new Map()
  );

// grouping by key

const groupBy = (data, keySelector, valueSelector = (x) => x) => {
  const map = new Map();

  for (const obj of data) {
    const key = keySelector(obj);
    const value = valueSelector(obj);

    if (!map.has(key)) {
      map.set(key, []);
    }

    map.get(key).push(value);
  }

  return map;
};

// groups of objects

// groups with specified values

// * TESTING

// count occurrences
console.log(countOccurrences([1, 2, 3, 4, 3, 2, 1]));

// group by

const cities = [
  { country: 'BG', city: 'Sofia' },
  { country: 'UK', city: 'London' },
  { country: 'UK', city: 'Manchester' },
  { country: 'BG', city: 'Plovdiv' },
  { country: 'US', city: 'Chicago' },
];

// group by coutnry
// console.log(groupBy(cities, (x) => x.country));

// group by country a
console.log(
  groupBy(
    cities,
    (x) => x.country,
    (x) => x.city
  )
);
