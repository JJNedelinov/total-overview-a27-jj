class HashEntry {
  #hash;
  #value;
  #operationalLength;

  constructor(hash, value, length) {
    this.#hash = hash;
    this.#value = value;
    this.#operationalLength = length;
  }

  get hash() {
    return this.#hash;
  }

  get value() {
    return this.#value;
  }

  get operationalLength() {
    return this.#operationalLength;
  }
}

export default HashEntry;
