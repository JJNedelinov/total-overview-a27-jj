import Node from './node-class.js';

class BinarySearchTree {
  constructor() {
    this.root = null;
  }

  /**
   *
   * @param {Node} node
   * @returns
   */
  insert(value) {
    const node = new Node(value);

    if (!this.root) {
      this.root = node;
      return this;
    }

    // go right
    if (node.value > this.root.value) {
      // no node to te right
      if (!this.root.right) {
        this.root.right = node;
        return this;
      }

      const nextNode = this.root.right;
      return this.insertionHelper(node, nextNode);
    }

    // go left

    // no node to te left
    if (!this.root.left) {
      this.root.left = node;
      return this;
    }

    const nextNode = this.root.left;
    return this.insertionHelper(node, nextNode);
  }

  breadthFirstSearch() {
    const queue = [];
    const visited = [];

    if (!this.root) return null;

    queue.push(this.root);

    while (queue.length) {
      const dequeued = queue.shift();

      visited.push(dequeued.value);

      if (dequeued.left) queue.push(dequeued.left);
      if (dequeued.right) queue.push(dequeued.right);
    }

    return visited;
  }

  dfsPreOrder() {
    const visited = [];
    const current = this.root;

    /**
     *
     * @param {Node} node
     */
    const traverse = (node) => {
      visited.push(node.value);

      if (node.left) traverse(node.left);
      if (node.right) traverse(node.right);
    };

    traverse(current);

    return visited;
  }

  dfsPostOrder() {
    const visited = [];
    const current = this.root;

    /**
     *
     * @param {Node} node
     */
    const traverse = (node) => {
      if (node.left) traverse(node.left);
      if (node.right) traverse(node.right);

      visited.push(node.value);
    };

    traverse(current);

    return visited;
  }

  dfsInOrder() {
    const visited = [];
    const current = this.root;

    /**
     *
     * @param {Node} node
     */
    const traverse = (node) => {
      if (node.left) traverse(node.left);

      visited.push(node.value);

      if (node.right) traverse(node.right);
    };

    traverse(current);

    return visited;
  }

  find(value) {
    if (!this.root) return false;

    if (this.root.value === value) {
      return this.root;
    }

    // go left if root has greater value
    if (this.root.value > value) {
      // if nothing on the left of the root - return false
      if (!this.root.left) return false;

      // if something on the left of the root - check for a value until end
      const nextNode = this.root.left;
      return this.findHelper(value, nextNode);
    }

    // go right if root has smaller value
    if (this.root.value < value) {
      // if nothing is on the right of the root - return false
      if (!this.root.right) return false;

      // if something on the right of the root - check for a value until end
      const nextNode = this.root.right;
      return this.findHelper(value, nextNode);
    }
  }

  findDepth() {
    let count = 0;
    const depths = [];
    const current = this.root;

    const traverse = (node) => {
      count++;
      depths.push(count);

      if (node.left) traverse(node.left);
      if (node.right) traverse(node.right);
      count--;
    };

    traverse(current);

    return Math.max(...depths);
  }

  /**
   *
   * @param {Node} node
   * @param {Node} nextNode
   * @returns The current Tree
   */
  insertionHelper(node, nextNode) {
    while (nextNode) {
      // check if values of node and next node are equal - if so, ignore
      if (node.value === nextNode.value) return false;

      // if we add duplicates, we could have a frequency/count to the node - something like 10(2), 16(4), etc

      // greater than next node's value
      if (node.value > nextNode.value) {
        if (!nextNode.right) {
          nextNode.right = node;
          return this;
        }

        nextNode = nextNode.right;
        continue;
      }

      // less than next node's value
      if (!nextNode.left) {
        nextNode.left = node;
        return this;
      }

      nextNode = nextNode.left;
    }
  }

  /**
   *
   * @param {number} value
   * @param {Node} nextNode
   * @returns The found node - if found / false - if not found
   */
  findHelper(value, nextNode) {
    while (nextNode) {
      // if the next node is equal to the value - return it
      if (nextNode.value === value) return nextNode;

      // if the next node is not equal to the value - keep searching

      // if next node's value is greater than the value - go left
      if (nextNode.value > value) {
        // if there's nothing on the left - return false
        if (!nextNode.left) return false;

        // make the next node - the node on the left
        nextNode = nextNode.left;
        continue;
      }

      // if next node's value is smaller than the value - go right

      // if there's nothing on the right - return false
      if (!nextNode.right) return false;

      // make the next node - the node on the right
      nextNode = nextNode.right;
    }
  }
}

export default BinarySearchTree;

const tree = new BinarySearchTree();

// first insertions
// tree.insert(5);
// tree.insert(3);
// tree.insert(7);

// left side second insertion
// tree.insert(4);

// right side second insertion
// tree.insert(6);

// secondary insertions (more focused on the right)
// tree.insert(8);
// tree.insert(15);
// tree.insert(11);
// tree.insert(14);
// tree.insert(20);

// secondary insertions (more focused on the left)
// tree.insert(-1);
// tree.insert(-3);
// tree.insert(-10);
// tree.insert(-20);
// tree.insert(0);

// * find a node by value in BST
// console.log(tree.find(-20));
// console.log(tree.find(14));
// console.log(tree.find(0));
// console.log(tree.find(13));

// * DFS - PreOrder

// * course tree for comparison

tree.insert(10);
tree.insert(6);
tree.insert(3);
tree.insert(8);
tree.insert(15);
tree.insert(20);

console.log(tree.dfsPreOrder());

// * DFS - PostOrder

console.log(tree.dfsPostOrder());

// * DFS - InOrder

console.log(tree.dfsInOrder());

// * DFS - Depth with DFS - PreOrder

console.log(tree.findDepth());
