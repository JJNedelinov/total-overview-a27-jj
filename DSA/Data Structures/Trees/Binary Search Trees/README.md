# Data Structures Overall

## What Do They Do?

  - Data structures are collections of:
    - Values;
    - The Relationship Among Them;
    - The Functions or Operations that can be Applied to the Data

## Why So Many?

  - Different data structures excel (*being very good at something*) at different things. Some are highly specialized, while others (like arrays) are more generally used.

## Why Care?

  - The more time WE spend as developers, the more likely we will need to use one of these data structures
  - We have already worked with many of them unknowingly
  - Interviews

# Trees 

## Objectives:

  - Define what a **tree** is
  - Compare and contrast **trees** and **lists**
  - Explain the **differences** between **trees**, **binary trees**, and **binary search trees**
  - Implement **operations** on **binary search trees**

## What is a Tree?