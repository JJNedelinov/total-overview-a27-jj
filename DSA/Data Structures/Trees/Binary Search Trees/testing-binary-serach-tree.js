import BinarySearchTree from './binary-search-tree-class.js';
import Node from './node-class.js';

const tree = new BinarySearchTree();
tree.root = new Node(10);
tree.root.right = new Node(15);
tree.root.left = new Node(5);
tree.root.left.right = new Node(7);
