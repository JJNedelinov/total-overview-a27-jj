import BinarySearchTree from '../Binary Search Trees/binary-search-tree-class.js';

/**
 *
 * @param {BinarySearchTree} tree
 * @returns List with the values of all nodes in the Tree
 */
const breadthFirstSearch = (tree) => {
  const queue = [];
  const visited = [];

  if (!tree.root) return null;

  queue.push(tree.root);

  while (queue.length) {
    const dequeued = queue.shift();

    visited.push(dequeued.value);

    if (dequeued.left) queue.push(dequeued.left);
    if (dequeued.right) queue.push(dequeued.right);
  }

  return visited;
};

const tree = new BinarySearchTree();

// first insertions
tree.insert(5);
tree.insert(3);
tree.insert(7);

// left side second insertion
tree.insert(4);

// right side second insertion
tree.insert(6);

// secondary insertions (more focused on the right)
tree.insert(8);
tree.insert(15);
tree.insert(11);
tree.insert(14);
tree.insert(20);

// secondary insertions (more focused on the left)
tree.insert(-1);
tree.insert(-3);
tree.insert(-10);
tree.insert(-20);
tree.insert(0);

// * call the Breadth-first Search Function

// const result = breadthFirstSearch(tree);
const result = tree.breadthFirstSearch();
console.log(result);
