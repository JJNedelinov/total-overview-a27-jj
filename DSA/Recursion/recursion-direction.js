const recursionDirection = (start, end) => {
  if (start === end) {
    return start;
  }

  // console.log(start);

  recursionDirection(start + 1, end);
  
  console.log(start);
}

console.log(recursionDirection(1, 5));