/**
 *
 * @param {string} str
 */

const stringReverse = (str) => {
  if (str.length === 1 || str.length === 0) {
    return str;
  }

  return str[str.length - 1] + stringReverse(str.slice(0, str.length - 1));
};

console.log(stringReverse('Telerik'));
