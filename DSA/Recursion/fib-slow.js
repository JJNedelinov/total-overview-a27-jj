const fibSlow = (num) => {
  if (num < 0)
    throw new Error(
      'Fibonnaci Sequence consists of only positive integers and zero'
    );
  
  if (num === 0) return 0;
  if (num === 1 || num === 2) return 1;

  return fibSlow(num - 1) + fibSlow(num - 2);
};

console.log(fibSlow(-1));
