/**
 *
 * @param {object} obj
 */

const collectStrings = (obj) => {
  const arrOfStrings = [];

  const helper = (obj) => {
    for (const key in obj) {
      if (obj[key] instanceof Object && !Array.isArray(obj[key])) {
        helper(obj[key]);
      } else {
        if (typeof obj[key] === 'string') {
          arrOfStrings.push(obj[key]);
        }
      }
    }
  };

  helper(obj);

  return arrOfStrings;
};

const obj = {
  stuff: 'foo',
  data: {
    val: {
      thing: {
        info: 'bar',
        moreInfo: {
          evenMoreInfo: {
            weMadeIt: 'baz',
          },
        },
      },
    },
  },
};

console.log(collectStrings(obj)); // ["foo", "bar", "baz"])
