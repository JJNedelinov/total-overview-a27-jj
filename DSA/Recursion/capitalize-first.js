/**
 *
 * @param {array} arrOfStrings
 */

const capitalizeFirst = (arrOfStrings) => {
  if (arrOfStrings.length <= 0) return [];

  return [
    arrOfStrings[0].slice(0)[0].toUpperCase() + arrOfStrings[0].slice(1),
  ].concat(capitalizeFirst(arrOfStrings.slice(1)));
};

console.log(capitalizeFirst(['car', 'taco', 'banana'])); // ['Car','Taco','Banana']
