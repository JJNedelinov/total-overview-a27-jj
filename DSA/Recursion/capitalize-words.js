/**
 *
 * @param {array} arrOfWords
 */

const capitalizeWords = (arrOfWords) => {
  if (arrOfWords.length === 0) return [];

  return [arrOfWords[0].slice(0).toUpperCase()].concat(
    capitalizeWords(arrOfWords.slice(1))
  );
};

let words = ['i', 'am', 'learning', 'recursion'];
console.log(capitalizeWords(words)); // ['I', 'AM', 'LEARNING', 'RECURSION']
