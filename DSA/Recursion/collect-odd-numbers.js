/**
 *
 * @param {array} nums
 * @param {array} list
 */

const collectOddNumbers = (nums, list = []) => {
  if (nums.length === 0) return;
  if (nums[0] % 2 !== 0) list.push(nums[0]);

  collectOddNumbers(nums.slice(1), list);

  return list.slice();
};

console.log(collectOddNumbers([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));
