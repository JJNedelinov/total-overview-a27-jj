const isPalindrome = (str) => {
  const reverse = (str) => {
    if (str.length === 0) return '';
    return reverse(str.slice(1)) + str[0];
  };

  const reversedStr = reverse(str);

  return str === reversedStr;
};

console.log(isPalindrome('awesome')); // false
console.log(isPalindrome('foobar')); // false
console.log(isPalindrome('tacocat')); // true
console.log(isPalindrome('amanaplanacanalpanama')); // true
console.log(isPalindrome('amanaplanacanalpandemonium')); // false
