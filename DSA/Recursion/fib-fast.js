const fibFast = (num, memo = {}) => {
  if (!memo[num]) {
    if (num <= 2) {
      memo[num] = 1;
    } else {
      memo[num] = fibFast(num - 1, memo) + fibFast(num - 2, memo);
    }
  }

  return memo[num];
};

console.log(fibFast(46));