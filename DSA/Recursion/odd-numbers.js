/**
 *
 * @param {array} list
 */

const areAllNumbersOdd = (list) => {
  if (list.length === 0) {
    return false;
  }

  if (list.length === 1) {
    return list[0] % 2 === 0 ? false : true;
  }

  if (list[0] % 2 === 0) return false;

  return areAllNumbersOdd(list.slice(1));
};

console.log(areAllNumbersOdd([5213, 4124, 6532]));
console.log(areAllNumbersOdd([5213]));
console.log(areAllNumbersOdd([5213, 4123]));
console.log(areAllNumbersOdd([5213, 4123, 4]));
