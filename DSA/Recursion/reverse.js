const reverse = (str) => {
  if (str.length === 0) return '';

  return reverse(str.slice(1, str.length)) + str[0];
};

console.log(reverse('Zhulien'));
