/**
 *
 * @param {array} arr
 */

const flatten = (arr) => {
  let newArr = [];

  for (const el of arr) {
    if (el instanceof Array) {
      newArr = newArr.concat(flatten(el));
    } else {
      newArr.push(el);
    }
  }

  return newArr;
};

console.log(flatten([1, 2, 3, [4, 5]])); // [1, 2, 3, 4, 5]
console.log(flatten([1, [2, [3, 4], [[5]]]])); // [1, 2, 3, 4, 5]
console.log(flatten([[1], [2], [3]])); // [1,2,3]
console.log(flatten([[[[1], [[[2]]], [[[[[[[3]]]]]]]]]])); // [1,2,3]
